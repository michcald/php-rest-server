<?php

abstract class Rest_Request_Auth_Abstract {

    abstract public function authorize(Rest_Request $request);

}