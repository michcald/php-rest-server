<?php

class Rest_Request_Auth_Basic extends Rest_Request_Auth_Abstract {

    private $users = array();

    public final function __construct() {

    }

    public final function addUser($username, $password) {
        $this->users[$username] = $password;
    }

    /**
     *
     * @param Rest_Request $request
     * @return boolean
     */
    public final function authorize(Rest_Request $request) {
        $inputUsername = $request->getServerVar('PHP_AUTH_USER');
        $inputPassword = $request->getServerVar('PHP_AUTH_PW');

        if(!$inputUsername || !$inputUsername) {
            return false;
        }

        if(!array_key_exists($inputUsername, $this->users)) {
            return false;
        }

        if($this->users[$inputUsername] != $inputPassword) {
            return false;
        }

        return true;
    }

}