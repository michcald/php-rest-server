<?php

class Rest_Request_Auth_Digest extends Rest_Request_Auth_Abstract {

    private $users = array();

    private $realm = null;

    public final function __construct($realm) {
        $this->realm = $realm;
    }

    public final function addUser($username, $password) {
        $this->users[$username] = $password;
    }

    /**
     *
     * @param Rest_Request $request
     * @return boolean
     */
    public final function authorize(Rest_Request $request) {
        if(!$request->getServerVar('PHP_AUTH_DIGEST')) {
            return false;
        }

        if(!$data = $this->httpDigestParse($request->getServerVar('PHP_AUTH_DIGEST'))) {
            return false;
        }

        if(!isset($this->users[$data['username']])) {
            return false;
        }

        // generate the valid response
        $A1 = md5($data['username'] . ':' . $this->realm . ':' . $this->users[$data['username']]);
        $A2 = md5($request->getServerVar('REQUEST_METHOD').':'.$data['uri']);
        $validResponse = md5($A1 . ':' . $data['nonce'] . ':' . $A2);

        if($data['response'] == $validResponse) {
            return true;
        }

        return false;
    }

    // function to parse the http auth header
    private function httpDigestParse($txt) {
        // protect against missing data
        $neededParts = array('username' => 1, 'realm' => 1, 'nonce' => 1,
            'uri' => 1, 'response' => 1, 'opaque' => 1);

        $data = array();
        $keys = implode('|', array_keys($neededParts));

        preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]*?)\2|([^\s,]+))@',
                $txt, $matches, PREG_SET_ORDER);

        foreach ($matches as $m) {
            $data[$m[1]] = $m[3] ? $m[3] : $m[4];
            unset($neededParts[$m[1]]);
        }

        return $neededParts ? false : $data;
    }

}