<?php

class Rest_Request_Uri {

    private $uri = null;

    /**
     *
     * @param string $uri
     */
    public final function __construct($uri) {
        $this->uri = $uri;
    }

    /**
     *
     * @return string
     */
    public final function getValue() {
        return $this->uri;
    }

    /**
     *
     * @param string $separator
     * @param int $index
     * @return string|null
     */
    public final function getChunk($separator, $index) {
        $chunks = explode($separator, $this->uri);
        if(count($chunks) < $index) {
            return null;
        }
        return $chunks[$index];
    }

}