<?php

class Rest_Request_Input_File {

    private $name = null,
            $type = null,
            $tmpName = null,
            $size = null;

    /**
     *
     * @var Rest_Request_Input_File_Error_Abstract
     */
    private $error = null;

    public final function __construct() {

    }

    /**
     *
     * @param type string
     * @return \Rest_Request_Input_File
     */
    public final function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string
     */
    public final function getName() {
        return $this->name;
    }

    /**
     *
     * @param type string
     * @return \Rest_Request_Input_File
     */
    public final function setTmpName($tmpName) {
        $this->tmpName = $tmpName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public final function getTmpName() {
        return $this->tmpName;
    }

    /**
     *
     * @param string $type
     * @return \Rest_Request_Input_File
     */
    public final function setType($type) {
        $this->type = $type;
        return $this;
    }

    /**
     *
     * @return string
     */
    public final function getType() {
        return $this->type;
    }

    /**
     *
     * @param string $size
     * @return \Rest_Request_Input_File
     */
    public final function setSize($size) {
        $this->size = $size;
        return $this;
    }

    /**
     *
     * @return int
     */
    public final function getSize() {
        return $this->size;
    }

    /**
     *
     * @param int $error
     * @return \Rest_Request_Input_File
     */
    public final function setError($error) {
        switch($error) {
            case 0:
                $this->error = new Rest_Request_Input_File_Error_Ok();
                break;
            case 1:
                $this->error = new Rest_Request_Input_File_Error_IniSize();
                break;
            case 2:
                $this->error = new Rest_Request_Input_File_Error_FormSize();
                break;
            case 3:
                $this->error = new Rest_Request_Input_File_Error_Partial();
                break;
            case 4:
                $this->error = new Rest_Request_Input_File_Error_NoFile();
                break;
            case 6:
                $this->error = new Rest_Request_Input_File_Error_NoTmpDir();
                break;
            case 7:
                $this->error = new Rest_Request_Input_File_Error_CantWrite();
                break;
            case 8:
                $this->error = new Rest_Request_Input_File_Error_Extension();
                break;
            default:
                throw new Rest_Exception_Request('Unknown upload file error');
        }

        return $this;
    }

    /**
     *
     * @return Rest_Request_Input_File_Error_Abstract
     */
    public final function getError() {
        return $this->error;
    }

    /**
     *
     * @return boolean
     */
    public final function isError() {
         if($this->error instanceof Rest_Request_Input_File_Error_Ok) {
             return false;
         }
         return true;
    }

    /**
     *
     * @param string $dir
     * @throws Rest_Exception_Request
     */
    public final function saveTo($dir) {
        if(!is_dir($dir)) {
            throw new Rest_Exception_Request("Invalid directory '$dir'");
        }
        move_uploaded_file($this->getTmpName(), $dir);
    }
}