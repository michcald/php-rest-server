<?php

class Rest_Request_Input_File_Error_Ok extends Rest_Request_Input_File_Error_Abstract {

    public final function getCode() {
        return 0;
    }

    public final function getMessage() {
        return 'There is no error, the file uploaded with success.';
    }

}