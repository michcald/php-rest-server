<?php

class Rest_Request_Input_File_Error_NoFile extends Rest_Request_Input_File_Error_Abstract {

    public final function getCode() {
        return 4;
    }

    public final function getMessage() {
        return 'No file was uploaded.';
    }

}