<?php

class Rest_Request_Input_File_Error_CantWrite extends Rest_Request_Input_File_Error_Abstract {

    public final function getCode() {
        return 7;
    }

    public final function getMessage() {
        return 'Failed to write file to disk. Introduced in PHP 5.1.0.';
    }

}