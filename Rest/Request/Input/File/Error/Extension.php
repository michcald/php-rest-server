<?php

class Rest_Request_Input_File_Error_Extension extends Rest_Request_Input_File_Error_Abstract {

    public final function getCode() {
        return 8;
    }

    public final function getMessage() {
        return 'A PHP extension stopped the file upload. ' .
                'PHP does not provide a way to ascertain which extension ' .
                'caused the file upload to stop; examining the list of ' .
                'loaded extensions with phpinfo() may help. Introduced in PHP 5.2.0.';
    }

}