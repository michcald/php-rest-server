<?php

class Rest_Request_Input_File_Error_NoTmpDir extends Rest_Request_Input_File_Error_Abstract {

    public final function getCode() {
        return 6;
    }

    public final function getMessage() {
        return 'Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.';
    }

}