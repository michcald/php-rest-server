<?php

class Rest_Request_Input_File_Error_Partial extends Rest_Request_Input_File_Error_Abstract {

    public final function getCode() {
        return 3;
    }

    public final function getMessage() {
        return 'The uploaded file was only partially uploaded.';
    }

}