<?php

abstract class Rest_Request_Input_File_Error_Abstract {

    abstract public function getCode();

    abstract public function getMessage();

}