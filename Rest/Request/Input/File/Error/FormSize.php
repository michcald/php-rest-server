<?php

class Rest_Request_Input_File_Error_FormSize extends Rest_Request_Input_File_Error_Abstract {

    public final function getCode() {
        return 2;
    }

    public final function getMessage() {
        return 'The uploaded file exceeds the MAX_FILE_SIZE directive that ' .
                'was specified in the HTML form.';
    }

}