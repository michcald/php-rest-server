<?php

class Rest_Request_Input_File_Error_IniSize extends Rest_Request_Input_File_Error_Abstract {

    public final function getCode() {
        return 1;
    }

    public final function getMessage() {
        return 'The uploaded file exceeds the upload_max_filesize directive in php.ini.';
    }

}