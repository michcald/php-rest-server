<?php

class Rest_Request_Input_Param {

    private $value = null;

    public final function setValue($value) {
        $this->value = $value;
        return $this;
    }

    public final function getValue() {
        return $this->value;
    }

}