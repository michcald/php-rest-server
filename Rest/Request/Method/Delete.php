<?php

class Rest_Request_Method_Delete extends Rest_Request_Method_Abstract {

    public final function getValue() {
        return 'delete';
    }

}