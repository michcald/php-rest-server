<?php

class Rest_Request_Method_Options extends Rest_Request_Method_Abstract {

    public final function getValue() {
        return 'options';
    }

}