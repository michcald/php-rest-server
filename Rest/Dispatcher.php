<?php

class Rest_Dispatcher {

    public final function __construct() {

    }

    /**
     *
     * @param Rest_Request $request
     * @param Rest_Response $response
     * @throws Rest_Exception_Dispatcher
     */
    public final function dispatch(Rest_Request $request, Rest_Response $response) {
        try {
            $this->doDispatch($request, $response);
        } catch(Rest_Exception_Dispatcher $d) {
            throw $d;
        } catch(Exception $e) {
            throw new Rest_Exception_Dispatcher($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     *
     * @param Rest_Request $request
     * @param Rest_Response $response
     */
    public function doDispatch(Rest_Request $request, Rest_Response $response) {

    }

}