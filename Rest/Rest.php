<?php

class Rest_Rest {

    /**
     *
     * @var Rest_Request
     */
    private $request = null;

    /**
     *
     * @var Rest_Response
     */
    private $response = null;

    /**
     *
     * @var Rest_Router
     */
    private $router = null;

    /**
     *
     * @var Rest_Dispatcher
     */
    private $dispatcher = null;

    public final function __construct() {
        set_exception_handler(array('Rest_Exception_Handler', 'handle'));
    }

    public final function setRequest(Rest_Request $request) {
        $this->request = $request;
    }

    public final function setResponse(Rest_Response $response) {
        $this->response = $response;
    }

    public final function setRouter(Rest_Router $router) {
        $this->router = $router;
    }

    public final function setDispatcher(Rest_Dispatcher $dispatcher) {
        $this->dispatcher = $dispatcher;
    }

    public final function process() {
        if($this->request == null) {
            $this->request = new Rest_Request();
        }
        if($this->response == null) {
            $this->response = new Rest_Response();
        }
        if($this->router == null) {
            $this->router = new Rest_Router();
        }
        if($this->dispatcher == null) {
            $this->dispatcher = new Rest_Dispatcher();
        }

        $this->router->route($this->request);

        $this->dispatcher->dispatch($this->request, $this->response);

        Rest_Viewer::view($this->response);
    }
}