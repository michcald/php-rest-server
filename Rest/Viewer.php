<?php

abstract class Rest_Viewer {

    /**
     *
     * @param Rest_Response $response
     */
    public static final function view(Rest_Response $response) {

        if($response->getStatus()) {
            header('HTTP/1.0 ' .
                    $response->getStatus()->getCode() . ' ' .
                    $response->getStatus()->getMessage());
        }

        if($response->getContent()) {
            header('Content-type: ' . $response->getContent()->getType()->getValue());
            echo $response->getContent()->getBody();
        }
    }

}