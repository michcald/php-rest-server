<?php

class Rest_Response {

    /**
     *
     * @var Rest_Response_Status_Abstract
     */
    private $status = null;

    /**
     *
     * @var Rest_Response_Content
     */
    private $content = null;

    public function __construct() {

    }

    /**
     *
     * @param Rest_Response_Content $content
     * @return \Rest_Response
     */
    public final function setContent(Rest_Response_Content $content) {
        $this->content = $content;
        return $this;
    }

    /**
     *
     * @return Rest_Response_Content
     */
    public final function getContent() {
        return $this->content;
    }

    /**
     *
     * @param Rest_Response_Status_Abstract $status
     * @return \Rest_Response
     */
    public final function setStatus(Rest_Response_Status_Abstract $status) {
        $this->status = $status;
        return $this;
    }

    /**
     *
     * @return Rest_Response_Status_Abstract
     */
    public final function getStatus() {
        return $this->status;
    }

}