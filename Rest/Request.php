<?php

class Rest_Request {

    private $clientIdAddress = null,
            $time = null;

    /**
     *
     * @var Rest_Request_Method_Abstract
     */
    private $method = null;

    /**
     *
     * @var Rest_Request_Uri
     */
    private $uri = null;

    /**
     *
     * @var Rest_Response_Content_Type_Abstract
     */
    private $responseType = null;

    /**
     *
     * @var Rest_Request_Input_Param[]
     */
    private $params = array();

    /**
     *
     * @var Rest_Request_Input_File[]
     */
    private $files = array();

    public final function __construct() {

    }

    /**
     *
     * @param string $key
     * @return string
     */
    public final function getServerVar($key, $default = false) {
        if(isset($_SERVER[$key])) {
            return $_SERVER[$key];
        }
        return $default;
    }

    /**
     *
     * @param string $ip
     * @return \Rest_Request
     */
    public final function setIpAddress($ip) {
        $this->clientIdAddress = $ip;
        return $this;
    }

    /**
     *
     * @return string
     */
    public final function getIpAddress() {
        return $this->clientIdAddress;
    }

    /**
     *
     * @param string $time
     * @return \Rest_Request
     */
    public final function setTime($time) {
        $this->time = $time;
        return $this;
    }

    /**
     *
     * @return string
     */
    public final function getTime() {
        return $this->time;
    }

    /**
     *
     * @param Rest_Request_Method_Abstract $method
     * @return \Rest_Request
     */
    public final function setMethod(Rest_Request_Method_Abstract $method) {
        $this->method = $method;
        return $this;
    }

    /**
     *
     * @return Rest_Request_Method_Abstract
     */
    public final function getMethod() {
        return $this->method;
    }

    /**
     *
     * @param Rest_Request_Uri $uri
     * @return \Rest_Request
     */
    public final function setUri(Rest_Request_Uri $uri) {
        $this->uri = $uri;
        return $this;
    }

    /**
     *
     * @return Rest_Request_Uri
     */
    public final function getUri() {
        return $this->uri;
    }

    /**
     *
     * @param Rest_Response_Content_Type_Abstract $type
     * @return \Rest_Request
     */
    public final function setResponseType(Rest_Response_Content_Type_Abstract $type) {
        $this->responseType = $type;
        return $this;
    }

    /**
     *
     * @return Rest_Response_Content_Type_Abstract
     */
    public final function getResponseType() {
        return $this->responseType;
    }

    /**
     *
     * @param string $key
     * @param Rest_Request_Input_Param $param
     * @return \Rest_Request
     */
    public final function addParam($key, Rest_Request_Input_Param $param) {
        $this->params[$key] = $param;
        return $this;
    }

    public final function getParams() {
        return $this->params;
    }

    /**
     *
     * @param string $key
     * @param mixed $default
     * @return \Rest_Request_Input_Param
     */
    public final function getParam($key, $default = false) {
        if(array_key_exists($key, $this->params)) {
            return $this->params[$key];
        }
        $param = new Rest_Request_Input_Param();
        $param->setValue($default);
        return $param;
    }

    /**
     *
     * @param string $key
     * @param Rest_Request_Input_File $data
     * @return \Rest_Request
     */
    public final function addFile($key, Rest_Request_Input_File $file) {
        $this->files[$key] = $file;
        return $this;
    }

    /**
     *
     * @param string $key
     * @return Rest_Request_Input_File
     */
    public final function getFile($key) {
        return array_key_exists($key, $this->files) ? $this->files[$key] : null;
    }
}