<?php

class Rest_Router {

    public final function __construct() {

    }

    private static final function getWorkingDir() {
        $info = pathinfo($_SERVER['PHP_SELF']);
        return $info['dirname'];
    }

    private function initRequest(Rest_Request $request) {
        $request->setIpAddress($_SERVER['REMOTE_ADDR'])
                ->setTime($_SERVER['REQUEST_TIME']);

        $uri = preg_replace("#\?.*$#", '', $_SERVER['REQUEST_URI']);
        $uri = str_replace(self::getWorkingDir() . '/', '', $uri);
        $uri = new Rest_Request_Uri($uri);
        $request->setUri($uri);

        $method = null;
        $params = array();

        switch(strtoupper($_SERVER['REQUEST_METHOD'])) {
            case 'POST':
                $method = new Rest_Request_Method_Post();
                $params = $_POST;
                break;
            case 'GET':
                $method = new Rest_Request_Method_Get();
                $params = $_GET;
                break;
            case 'DELETE':
                $method = new Rest_Request_Method_Delete();
                parse_str(file_get_contents("php://input"), $params);
                break;
            case "PUT":
                $method = new Rest_Request_Method_Put();
                parse_str(file_get_contents("php://input"), $params);
                break;
            case "OPTIONS":
                $method = new Rest_Request_Method_Options();
                $params = $_GET;
                break;
            default:
                throw new Rest_Exception_Router(
                        "Method '" . $_SERVER['REQUEST_METHOD'] . "' not valid");
        }

        if(count($_FILES) > 0) {
            foreach($_FILES as $k => $v) {
                $file = new Rest_Request_Input_File();
                $file->setName($v['name'])
                        ->setSize($v['size'])
                        ->setTmpName($v['tmp_name'])
                        ->setType($v['type'])
                        ->setError($v['error']);
                $request->addFile($k, $file);
            }
        }

        $request->setMethod($method);
        foreach($params as $k => $v) {
            $param = new Rest_Request_Input_Param();
            $param->setValue($v);
            $request->addParam($k, $param);
        }
    }

    /**
     *
     * @param Rest_Request $request
     * @throws Rest_Exception_Router
     */
    public final function route(Rest_Request $request) {

        try {

            $this->initRequest(&$request);

            $this->doRoute(&$request);

        } catch(Rest_Exception_Router $r) {
            throw $r;
        } catch(Exception $e) {
            throw new Rest_Exception_Router($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     *
     * @param Rest_Request $request
     */
    public function doRoute(Rest_Request $request) {

    }
}