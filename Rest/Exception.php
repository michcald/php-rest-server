<?php

class Rest_Exception extends Exception {

    public function setMessage($message) {
        $this->message = $message;
    }

    public function setLine($line) {
        $this->line = (int)$line;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function setCode($code) {
        $this->code = $code;
    }

}