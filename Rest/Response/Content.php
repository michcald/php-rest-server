<?php

class Rest_Response_Content {

    /**
     *
     * @var Rest_Response_Content_Type_Abstract
     */
    private $type = null;

    /**
     *
     * @var string
     */
    private $body = null;

    /**
     *
     * @param Rest_Response_Content_Type_Abstract $type
     */
    public final function __construct(Rest_Response_Content_Type_Abstract $type) {
        $this->type = $type;
    }

    /**
     *
     * @return Rest_Response_Content_Abstract
     */
    public final function getType() {
        return $this->type;
    }

    /**
     *
     * @param string $body
     * @return \Rest_Response_Content
     */
    public final function setBody($body) {
        $this->body = $this->type->format($body);
        return $this;
    }

    /**
     *
     * @return string
     */
    public final function getBody() {
        return $this->body;
    }

}