<?php

abstract class Rest_Response_Content_Type_Abstract {

    abstract public function getValue();

    abstract public function format($data);

}