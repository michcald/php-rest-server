<?php

class Rest_Response_Content_Type_Xml extends Rest_Response_Content_Type_Abstract {

    /**
     *
     * @return string
     */
    public final function getValue() {
        return 'text/xml';
    }

    /**
     *
     * @param array $data
     * @return string
     */
    public final function format($data) {
        if(!is_array($data)) {
            throw new Rest_Exception_Response('Needs an array');
        }

        $temp = array();

        foreach($data as $key => $value) {
            $temp[$key] = $value;
            //$temp[$key]['object'] = $value;
        }

        $xml = new SimpleXMLElement("<?xml version=\"1.0\"?><list></list>");
        $this->arrayToXml($temp, $xml);

        return $xml->asXML();
    }

    private final function arrayToXml($array, &$output) {
        foreach($array as $key => $value) {
            if(is_array($value)) {
                if(!is_numeric($key)) {
                    $subnode = $output->addChild("$key");
                    $this->arrayToXml($value, $subnode);
                } else {
                    $this->arrayToXml($value, $output);
                }
            } else {
                $output->addChild("$key","$value");
            }
        }
    }

}