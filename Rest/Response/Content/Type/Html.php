<?php

class Rest_Response_Content_Type_Html extends Rest_Response_Content_Type_Abstract {

    /**
     *
     * @return string
     */
    public final function getValue() {
        return 'text/html';
    }

    /**
     *
     * @param string $data
     * @return string
     */
    public final function format($data) {
        if(is_array($data)) {
            return print_r($data, true);
        }
        return $data;
    }

}