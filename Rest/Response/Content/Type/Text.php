<?php

class Rest_Response_Content_Type_Text extends Rest_Response_Content_Type_Abstract {

    /**
     *
     * @return string
     */
    public final function getValue() {
        return 'plain/text';
    }

    /**
     *
     * @param string $data
     * @return string
     */
    public final function format($data) {
        if(is_array($data)) {
            return print_r($data, true);
        }
        return $data;
    }

}