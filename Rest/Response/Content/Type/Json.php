<?php

class Rest_Response_Content_Type_Json extends Rest_Response_Content_Type_Abstract {

    /**
     *
     * @return string
     */
    public final function getValue() {
        return 'text/json';
    }

    /**
     *
     * @param array $data
     * @return string
     */
    public final function format($data) {
        if(!is_array($data)) {
            throw new Rest_Exception_Response('Needs an array');
        }
        return json_encode($data);
    }

}