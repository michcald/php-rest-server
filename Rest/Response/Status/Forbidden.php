<?php

class Rest_Response_Status_Forbidden extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 403;
    }

    public function getMessage() {
        return 'Forbidden';
    }
}