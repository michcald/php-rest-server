<?php

class Rest_Response_Status_SeeOther extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 303;
    }

    public function getMessage() {
        return 'See other';
    }
}