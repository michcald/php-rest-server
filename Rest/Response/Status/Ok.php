<?php

class Rest_Response_Status_Ok extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 200;
    }

    public function getMessage() {
        return 'Ok';
    }
}