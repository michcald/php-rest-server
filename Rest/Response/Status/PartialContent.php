<?php

class Rest_Response_Status_PartialContent extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 206;
    }

    public function getMessage() {
        return 'Partial content';
    }
}