<?php

class Rest_Response_Status_BadRequest extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 400;
    }

    public function getMessage() {
        return 'Bad request';
    }
}