<?php

class Rest_Response_Status_MovedPermanently extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 301;
    }

    public function getMessage() {
        return 'Moved permanently';
    }
}