<?php

class Rest_Response_Status_TemporaryRedirect extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 307;
    }

    public function getMessage() {
        return 'Temporary redirect';
    }
}