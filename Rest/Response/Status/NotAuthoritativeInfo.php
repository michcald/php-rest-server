<?php

class Rest_Response_Status_NotAuthoritativeInfo extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 203;
    }

    public function getMessage() {
        return 'Not authoritative info';
    }
}