<?php

class Rest_Response_Status_Conflict extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 409;
    }

    public function getMessage() {
        return 'Conflict';
    }
}