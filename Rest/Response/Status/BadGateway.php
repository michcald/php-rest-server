<?php

class Rest_Request_Status_BadGateway extends Rest_Request_Status_Abstract {

    public function getCode() {
        return 502;
    }

    public function getMessage() {
        return 'Bad gateway';
    }
}