<?php

class Rest_Response_Status_Continue extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 100;
    }

    public function getMessage() {
        return 'Continue';
    }
}