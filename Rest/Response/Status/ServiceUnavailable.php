<?php

class Rest_Response_Status_ServiceNotAvailable extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 503;
    }

    public function getMessage() {
        return 'Service not available';
    }
}