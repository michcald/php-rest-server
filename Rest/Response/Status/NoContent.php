<?php

class Rest_Response_Status_NoContent extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 204;
    }

    public function getMessage() {
        return 'No content';
    }
}