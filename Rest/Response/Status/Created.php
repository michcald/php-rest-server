<?php

class Rest_Response_Status_Created extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 201;
    }

    public function getMessage() {
        return 'Created';
    }
}