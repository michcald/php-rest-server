<?php

class Rest_Response_Status_UseProxy extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 305;
    }

    public function getMessage() {
        return 'Use Proxy';
    }
}