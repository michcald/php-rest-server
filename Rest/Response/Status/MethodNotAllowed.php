<?php

class Rest_Response_Status_MethodNotAllowed extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 405;
    }

    public function getMessage() {
        return 'Method not allowed';
    }
}