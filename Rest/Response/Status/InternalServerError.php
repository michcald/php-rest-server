<?php

class Rest_Response_Status_InternalServerError extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 500;
    }

    public function getMessage() {
        return 'Internal server error';
    }
}