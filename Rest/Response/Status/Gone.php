<?php

class Rest_Response_Status_Gone extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 410;
    }

    public function getMessage() {
        return 'Gone';
    }
}