<?php

class Rest_Response_Status_Unauthorized extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 401;
    }

    public function getMessage() {
        return 'Unauthorized';
    }
}