<?php

class Rest_Response_Status_ResetContent extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 205;
    }

    public function getMessage() {
        return 'Reset content';
    }
}