<?php

class Rest_Response_Status_ProxyAuthenticationRequired extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 407;
    }

    public function getMessage() {
        return 'Proxy authentication required';
    }
}