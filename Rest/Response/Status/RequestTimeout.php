<?php

class Rest_Response_Status_RequestTimeout extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 408;
    }

    public function getMessage() {
        return 'Request timeout';
    }
}