<?php

class Rest_Response_Status_RequestUriTooLong extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 414;
    }

    public function getMessage() {
        return 'Request URI too long';
    }
}