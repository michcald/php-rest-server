<?php

class Rest_Response_Status_SwitchingProtocol extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 101;
    }

    public function getMessage() {
        return 'Switching protocol';
    }
}