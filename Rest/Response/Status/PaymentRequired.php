<?php

class Rest_Response_Status_PaymentRequired extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 402;
    }

    public function getMessage() {
        return 'Payment required';
    }
}