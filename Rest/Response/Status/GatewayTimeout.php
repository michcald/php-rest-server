<?php

class Rest_Response_Status_GatewayTimeout extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 504;
    }

    public function getMessage() {
        return 'Gateway timeout';
    }
}