<?php

class Rest_Response_Status_PreconditionFailed extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 412;
    }

    public function getMessage() {
        return 'Precondition failed';
    }
}