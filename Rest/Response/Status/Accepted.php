<?php

class Rest_Response_Status_Accepted extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 202;
    }

    public function getMessage() {
        return 'Accepted';
    }
}