<?php

class Rest_Response_Status_RequestEntityTooLarge extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 413;
    }

    public function getMessage() {
        return 'Request entity too large';
    }
}