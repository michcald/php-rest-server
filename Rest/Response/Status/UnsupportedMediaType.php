<?php

class Rest_Response_Status_UnsupportedMediaType extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 415;
    }

    public function getMessage() {
        return 'Unsupported media type';
    }
}