<?php

class Rest_Response_Status_RequestedRangeNotSatisfiable extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 416;
    }

    public function getMessage() {
        return 'Requested range not satisfiable';
    }
}