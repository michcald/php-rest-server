<?php

class Rest_Response_Status_LengthRequired extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 411;
    }

    public function getMessage() {
        return 'Length required';
    }
}