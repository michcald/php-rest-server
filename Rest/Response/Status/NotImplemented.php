<?php

class Rest_Response_Status_NotImplemented extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 501;
    }

    public function getMessage() {
        return 'NotImplemented';
    }
}