<?php

class Rest_Response_Status_Found extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 302;
    }

    public function getMessage() {
        return 'Found';
    }
}