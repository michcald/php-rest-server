<?php

class Rest_Response_Status_ExpectationFailed extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 417;
    }

    public function getMessage() {
        return 'Expectation failed';
    }
}