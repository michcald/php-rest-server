<?php

abstract class Rest_Response_Status_Abstract {

    abstract public function getCode();

    abstract public function getMessage();

}