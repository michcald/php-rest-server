<?php

class Rest_Response_Status_Unused extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 306;
    }

    public function getMessage() {
        return 'Unused';
    }
}