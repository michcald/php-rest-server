<?php

class Rest_Response_Status_NotModified extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 404;
    }

    public function getMessage() {
        return 'Not modified';
    }
}