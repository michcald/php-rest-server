<?php

class Rest_Response_Status_MultipleChoises extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 300;
    }

    public function getMessage() {
        return 'Multiple choises';
    }
}