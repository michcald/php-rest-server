<?php

class Rest_Response_Status_NotAcceptable extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 406;
    }

    public function getMessage() {
        return 'Not acceptable';
    }
}