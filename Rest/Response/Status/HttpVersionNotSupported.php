<?php

class Rest_Response_Status_HttpVersionNotSupported extends Rest_Response_Status_Abstract {

    public function getCode() {
        return 505;
    }

    public function getMessage() {
        return 'HTTP version not supported';
    }
}