<?php

class MyClass_Dispatcher extends Rest_Dispatcher {

    public function doDispatch(Rest_Request $request, Rest_Response $response) {

        // adding the basic authentication (digest available as well)
        $auth = new Rest_Request_Auth_Basic();
        // defining the users credentials
        $auth->addUser('john', 'doe');
        $auth->addUser('matt', 'smith');

        // verifying the auth params from the request

        // if not define a not authorized status for the response
        if(!$auth->authorize($request)) {
            $response->setStatus(new Rest_Response_Status_Unauthorized());
            return;
        }

        // if authorized define the content of the response
        $content = new Rest_Response_Content(new Rest_Response_Content_Type_Json());
        $content->setBody(array(
            'time' => $request->getTime(),
            'uri' => $request->getUri()->getValue(),
            'params' => $request->getParams()
        ));

        $response->setContent($content);
    }

}