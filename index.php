<?php

// Useful autoloading function
function __autoload($className) {
    $fileName = str_replace('_', '/', $className) . '.php';
    if(is_file($fileName)) {
        include $fileName;
    }
}

$rest = new Rest_Rest();

// The framework implements its own versions of:
// - request
// - response
// - router
// - dispatcher
// It is possible to customize all of them extending the respective classes
// and using the setter methods before the process one

$rest->setDispatcher(new MyClass_Dispatcher());

$rest->process();
